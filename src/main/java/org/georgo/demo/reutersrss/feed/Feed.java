package org.georgo.demo.reutersrss.feed;

import java.util.ArrayList;
import java.util.List;

/**
 * Feed class
 * @author Tomas K. <iam@tomask.info>
 *
 */
public class Feed {
    final String title;
    final String description;
    final String copyright;
    final String pubDate;
    final String link;
    final List<FeedItem> items = new ArrayList<FeedItem>();

    public Feed(String title, String description, String copyright, String pubDate,
            String link) {
          this.title = title;
          this.description = description;
          this.copyright = copyright;
          this.pubDate = pubDate;
          this.link = link;
        }
    
    public String getTitle() {
        return title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public String getCopyright() {
        return copyright;
    }
    
    public String getPubDate() {
        return pubDate;
    }
    
    public String getLink() {
        return link;
    }
    
    public List<FeedItem> getItems() {
        return items;
      }
    
    @Override
    public String toString() {
        return title + " (" + copyright + ") [" + link + "]: "+ description;
    }
}
