package org.georgo.demo.reutersrss;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.georgo.demo.reutersrss.feed.Feed;
import org.georgo.demo.reutersrss.feed.FeedItem;
import org.georgo.demo.reutersrss.feed.FeedParser;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

/**
 * Servlet that servers feed in json format to display with ajax
 * @author Tomas K. <iam@tomask.info>
 *
 */
@SuppressWarnings("serial")
public class ReutersrssServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {
        if (req.getParameter("url") != null) {
            resp.setContentType("application/json");
            // Download and parse required feed from url
            FeedParser feedParser = new FeedParser(req.getParameter("url"));
            // Get parsed feed
            Feed feed = feedParser.readFeed();
            // Print items
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("title", feed.getTitle());
                jsonObj.put("description", feed.getDescription());
                JSONArray itemsArray = new JSONArray();
                for (FeedItem item: feed.getItems()) {
                    JSONObject itemArray = new JSONObject();
                    itemArray.put("title", item.getTitle());
                    itemArray.put("description", item.getDescription());
                    itemArray.put("link", item.getLink());
                    itemArray.put("date", item.getPubDate());
                    itemsArray.put(itemArray);
                }
                jsonObj.put("items", itemsArray);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            resp.getWriter().print(jsonObj);
        } else {
            resp.setContentType("text/plain");
            resp.getWriter().println("You must define _url_ parameter with RSS feed");
        }
    }
}
